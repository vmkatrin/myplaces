//
//  PlaceModel.swift
//  MyPlaces
//
//  Created by Katty on 27.10.2020.
//

import RealmSwift

class Place: Object {
    @objc dynamic var name = ""
    @objc dynamic var location: String?
    @objc dynamic var type: String?
    @objc dynamic var imageData: Data?
    @objc dynamic var date = Date()
    @objc dynamic var rating = 0.0
    
    convenience init(name:String, location: String?, type: String?, imageData: Data?, rating: Double) {
        self.init()
        self.name = name
        self.location = location
        self.type = type
        self.imageData = imageData
        self.rating = rating
    }
    
//    let restaurantNames = [
//        "Burger Heroes", "Kitchen", "Bonsai", "Дастархан",
//        "Индокитай", "X.O", "Балкан Гриль", "Sherlock Holmes",
//        "Speak Easy", "Morris Pub", "Вкусные истории",
//        "Классик", "Love&Life", "Шок", "Бочка"
//    ]
//    сохранение и выведение на экран
//    static func getPlaces() -> [Place] {
//        var places  = [Place]()
//
//        for place in restaurantNames {
//            places.append(Place(name: place, location: "Киев", type: "Ресторан", image: nil, restrauntImage: place))
//        }
//
//        return places
//    }

//    func savePlaces() {
//        for place in restaurantNames {
//            let image = UIImage(named:place)
//            guard let imageData = image?.pngData() else { return }
//            let newPlace = Place()
//            
//            newPlace.name = place
//            newPlace.location = "Киев"
//            newPlace.type = "Ресторан"
//            newPlace.imageData = imageData
//            
//            StorageManager.saveObject(newPlace)
//        }
//    }
}
